package Task5_Football;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;



public class Football_3 {
	public static void main(String[] args) {
	    List<Football_Club> table = Arrays.asList(
	        new Football_Club(1, "Manchester United", 11, 25, 0, 0, 12,0,45,12,6, 24,8,1),
	        new Football_Club(2, "Liverpool", 11, 21, 2, 2, 10,2,25,14,0,9,6,3),
	        new Football_Club(3, "Arsenal", 11, 4, 1, 3, 14,1,25,21,0,0,6,3),
	        new Football_Club(4, "Everton", 11, 17, 4, 4, 8,4,17,28,0,0,1,2),
	        new Football_Club(5, "Chelsea", 11, 7,5,3,14,2,31,26,3,3,3,2),
	        new Football_Club(6, "Wolverhampton Wanderers", 11, 4, 2, 5, 14,2,21,26,3,6,5,1),
	        new Football_Club(7, "Blackburn Rovers", 11, 15, 3, 5, 16,4,36,25,0,0,3,1),
	        new Football_Club(8, "Burnley", 11, 20, 2, 4, 9,2,34,16,3,9,2,5),
	        new Football_Club(9, "Ipswich Town", 11, 2, 4, 5, 11,1,13,12,0,0,4,1),
	        new Football_Club(10, "Southampton", 11, 3, 6, 1, 26,1,10,13,3,0,3,2),
	        new Football_Club(11, "Oldham Athletic", 11, 8, 2, 6, 17, 1,14,17,0,3,1,3),
	        new Football_Club(12, "Derby Country", 11, 15, 0, 8, 12,0,34,0,9,17,12,6));
	    
	    OptionalInt fb_min = table.stream().mapToInt(Football_Club::getGoals_Scored).min();
	    if (fb_min.isPresent()) {
	      System.out.printf("Lowest number of points scored by a team %d\n", fb_min.getAsInt());
	    } else {
	      System.out.println("min failed");
	    }
	    
	    OptionalInt fb_max = table.stream().mapToInt(Football_Club::getGoals_Scored).max();
	    if (fb_max.isPresent()) {
	    	System.out.printf("Highest number of points scored by a team : %d\n", fb_max.getAsInt());
	    } else {
	    	System.out.println("max failed");
	    }
	    
	    // reduce
	    System.out.println("Total matches won:");
	    Integer fb_output = table.stream().map(Football_Club::getWon).reduce(0, (a, b) -> a + b);
	    System.out.println(fb_output);
	    
	    //Collectors
	    List<String> fb_result = table.stream().filter(p -> p.getLong_range_goals() > 8).map(Football_Club::getClub)
	    		.collect(Collectors.toList());
	    System.out.println(fb_result.toString());
	  
	    try {
	        FileWriter writer = new FileWriter("Football_OUTPUT3.txt");
	        writer.write("Lowest number of points scored by a team:" + fb_min.getAsInt() + "\n");
	        writer.write("Highest number of points scored by a team: " + fb_max + "\n");
	        writer.write("Total matches won: " + fb_output + "\n");
	        writer.write("Teams who put three pointers: " + fb_result + "\n");
	        writer.close();
	        System.out.println("\nFootball OUTPUT3.txt file successfully written.");
	      } catch (IOException e) {
	        System.out.println("An error has occurred.");
	        e.printStackTrace();
	      }
	  }

	 
	  }


