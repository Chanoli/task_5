package Task5_Football;

public class Football_Club implements Comparable<Football_Club>{
	  private int placements;
	  private String fbclub_names;
	  private int matches_played;
	  private int fb_matches_won;
	  private int fb_matches_drawn;
	  private int fb_matches_lost;
	  private int Goals_Scored;
	  private int Sent_off;
	  private int total_won;
	  private int total_lost;
	  private int Midfield;
	  private int Long_range_goals;
	  private int Penalty_Kicks_Converted;
	  private int Fouls_Committed;
	  
	  public Football_Club(int placements, String fbclub_names, int matches_played, 
			         int fb_matches_won, int fb_matches_drawn, int fb_matches_lost, 
			         int Goals_Scored, int Sent_off, int total_won, int total_lost, 
			         int Midfield, int Long_range_goals, int Penalty_Kicks_Converted, int Fouls_Committed) {
		  
		 this.placements = placements;
		 this.fbclub_names = fbclub_names;
		 this.matches_played = matches_played;
		 this.fb_matches_won = fb_matches_won;
		 this.fb_matches_drawn = fb_matches_drawn;
		 this.fb_matches_lost = fb_matches_lost;
		 this.Goals_Scored = Goals_Scored;
		 this.Sent_off = Sent_off;
		 this.total_won = total_won;
		 this.total_lost = total_lost;
		 this.Midfield = Midfield;
		 this.Long_range_goals = Long_range_goals;
		 this.Penalty_Kicks_Converted = Penalty_Kicks_Converted;
		 this.Fouls_Committed = Fouls_Committed;
	  }
	  
	  public String toString() {
		  return String.format("%-3d%-20s%10d%10d%10d%10d", placements, fbclub_names, Goals_Scored, 
				  total_won, total_lost,fb_matches_drawn);	  

}
	  public int getPosition() {
		    return placements;
		  }

	  public void setPosition(int position) {
		    this.placements = position;
		  }
	  
	  public String getClub() {
			    return fbclub_names;
			  }

	  public void setClub(String club) {
			    this.fbclub_names = club;
			  } 
	  
	  public int getPlayed() {
		    return matches_played;
		  }

	  public void setPlayed(int played) {
		    this.matches_played = played;
		  }
	  
	  public int getWon() {
		    return fb_matches_won;
		  }

	  public void setWon(int won) {
		    this.fb_matches_won = won;
		  }
	  
	  public int getDrawn() {
		    return fb_matches_drawn;
		  }

	  public void setDrawn(int drawn) {
		    this.fb_matches_drawn = drawn;
		  }
	  
	  public int getLost() {
		    return fb_matches_lost;
		  }

	  public void setLost(int lost) {
		    this.fb_matches_lost = lost;
		  }
	  
	  public int getMidfield() {
		    return Midfield;
		  }

	  public void setMidfield(int Midfield) {
		    this.Midfield = Midfield;
		  }
	  
	  public int getGoals_Scored() {
		    return Goals_Scored;
	  }

	  public void setGoals_Scored(int Goals_Scored) {
		    this.Goals_Scored = Goals_Scored;
	  }
	  
	  public int getSent_off() {
		    return Sent_off;
		  }

	  public void setSent_off(int Sent_off) {
		    this.Sent_off = Sent_off;
		  }
	  
	  public int total_won() {
		    return total_won;
		  }

	  public void settotalwon(int totalwon) {
		    this.total_won = totalwon;
		  }
	  
	  public int gettotallost() {
		    return total_lost;
		  }

	  public void settotallost(int totallost) {
		    this.total_lost = totallost;
		  }
	  
	  public int getLong_range_goals() {
		    return Long_range_goals;
		  }

	  public void setLong_range_goals(int Long_range_goals) {
		    this.Long_range_goals = Long_range_goals;
		  }
	  
	  public int getPenalty_Kicks_Converted() {
		    return Penalty_Kicks_Converted;
		  }

	  public void setPenalty_Kicks_Converted(int Penalty_Kicks_Converted) {
		    this.Penalty_Kicks_Converted = Penalty_Kicks_Converted;
		  }
	  
	  public int getFouls_Committed() {
		    return Fouls_Committed;
		  }

	  public void setFouls_Committed(int Fouls_Committed) {
		    this.Fouls_Committed = Fouls_Committed;
		  }
	  
	  public int compareTo(Football_Club c) {
		    return ((Integer) Sent_off).compareTo(c.Sent_off);
		  }
		}

