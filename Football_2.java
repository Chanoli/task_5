package Task5_Football;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Football_2 {
	public static void main(String[] args) {
	    List<Football_Club> table = Arrays.asList(
	        new Football_Club(1, "Manchester United", 11, 25, 0, 0, 12,0,45,12,6, 24,8,1),
	        new Football_Club(2, "Liverpool", 11, 21, 2, 2, 10,2,25,14,0,9,6,3),
	        new Football_Club(3, "Arsenal", 11, 4, 1, 3, 14,1,25,21,0,0,6,3),
	        new Football_Club(4, "Everton", 11, 17, 4, 4, 8,4,17,28,0,0,1,2),
	        new Football_Club(5, "Chelsea", 11, 7,5,3,14,2,31,26,3,3,3,2),
	        new Football_Club(6, "Wolverhampton Wanderers", 11, 4, 2, 5, 14,2,21,26,3,6,5,1),
	        new Football_Club(7, "Blackburn Rovers", 11, 15, 3, 5, 16,4,36,25,0,0,3,1),
	        new Football_Club(8, "Burnley", 11, 20, 2, 4, 9,2,34,16,3,9,2,5),
	        new Football_Club(9, "Ipswich Town", 11, 2, 4, 5, 11,1,13,12,0,0,4,1),
	        new Football_Club(10, "Southampton", 11, 3, 6, 1, 26,1,10,13,3,0,3,2),
	        new Football_Club(11, "Oldham Athletic", 11, 8, 2, 6, 17, 1,14,17,0,3,1,3),
	        new Football_Club(12, "Derby Country", 11, 15, 0, 8, 12,0,34,0,9,17,12,6));
	    
	    
	    System.out.println("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
	    table.stream().forEach(b -> System.out.println(b));
	    System.out.println();
	    table.parallelStream().forEach(System.out::println);
	    
	    try {
			FileWriter writer = new FileWriter("Football_OUTPUT2.txt");
			writer.write("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
			writer.write("   ---------                ------    ---------  ---------- ------\n");
			table.stream().forEach(x -> {
			try {
				writer.write(x + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
			});
			
			writer.write("\n Parallel Stream \n");
			writer.write("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
			writer.write("   ---------                ------    ---------  ---------- ------\n");
			table.parallelStream().forEach(x -> {
				try {
					writer.write(x.toString() + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			writer.close();
			System.out.println("\nFootball OUTPUT2.txt file successfully written.");
	    } catch (IOException e) {
	    	System.out.println("An error has occurred.");
	    	e.printStackTrace();
	    }
	}


	}




