package Task5_Football;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



public class Football_GUI extends JFrame {
private JPanel contentPane;

  public Football_GUI() {
	setResizable(false);
	setTitle("6CS002_Task05_2064800");
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setSize(600, 600);
	setLocationRelativeTo(null);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);
	contentPane.setLayout(null);
	
	JLabel title = new JLabel("<html><h1><strong><i>Football</i></strong></h1><hr></html>");
	title.setBounds(210, 34, 45, 16);
	title.resize(300, 50);
	contentPane.add(title);
	
	JButton footbl1 = new JButton("FOOTBALL 01");
	footbl1.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			Football_1.main(null);
		}
	});
	footbl1.setBounds(200, 80, 117, 29);
	footbl1.resize(200, 50);
	contentPane.add(footbl1);
	
	JButton footbl2 = new JButton("FOOTBALL 02");
	footbl2.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			Football_2.main(null);
		}
	});
	footbl2.setBounds(200, 150, 117, 29);
	footbl2.resize(200, 50);
	contentPane.add(footbl2);
	
	JButton footbl3 = new JButton("FOOTBALL 03");
	footbl3.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			Football_3.main(null);
		}
	});
	footbl3.setBounds(200, 220, 117, 29);
	footbl3.resize(200, 50);
	contentPane.add(footbl3);
	
	JButton footbl4 = new JButton("FOOTBALL 04");
	footbl4.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			Football_4.main(null);
		}
	});
	footbl4.setBounds(200, 290, 117, 29);
	footbl4.resize(200, 50);
	contentPane.add(footbl4);
	
	JButton footbl5 = new JButton("FOOTBALL 05");
	footbl5.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			Football_5.main(null);
		}
	});
	footbl5.setBounds(200, 360, 117, 29);
	footbl5.resize(200, 50);
	contentPane.add(footbl5);
	
	JButton exit = new JButton("exit");
	exit.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	});
	exit.setBounds(200, 440, 117, 29);
	exit.resize(200, 50);
	contentPane.add(exit);
}
  public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Football_GUI frame = new Football_GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
